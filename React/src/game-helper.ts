export const BOARD_WIDTH = 6;
export const BOARD_HEIGHT = 12;

export interface Puyo {
  x: number;
  y: number;
  color: number;
}

export function UpdateBoard(board: number[][], piece: Puyo[], score: number): number {
  //check for combos if the piece is no longer falling
  if (((piece[0].y + 1) >= BOARD_HEIGHT || (piece[1].y + 1) >= BOARD_HEIGHT) ||
    (board[piece[0].x][piece[0].y + 1] !== 0 && board[piece[1].x][piece[1].y + 1] !== 0)) {
    score += CheckBoardForCombos(board);

    //generate new piece
    piece[0] = { x: 2, y: -1, color: Math.floor(Math.random() * 4 + 1) };
    piece[1] = { x: 3, y: -1, color: Math.floor(Math.random() * 4 + 1) };
  }
  else {
    //Piece falling Horizontally
    if (piece[0].y === piece[1].y) {
      if (board[piece[0].x][piece[0].y + 1] !== 0 && piece[0].color !== 0) {
        board[piece[0].x][piece[0].y] = piece[0].color;
        piece[0].color = 0;
      }
      if (board[piece[1].x][piece[1].y + 1] !== 0 && piece[1].color !== 0) {
        board[piece[1].x][piece[1].y] = piece[1].color;
        piece[1].color = 0;
      }
    }

    //Piece falling
    if (piece[0].color !== 0) {
      board[piece[0].x][piece[0].y] = 0;
      piece[0].y = piece[0].y + 1;
    }

    if (piece[1].color !== 0) {
      board[piece[1].x][piece[1].y] = 0;
      piece[1].y = piece[1].y + 1;
    }
  }

  return score;
}

export function MovePieceLeft(board: number[][], piece: Puyo[]) {
  //Check piece is not blocked by border or by another puyo
  if ((piece[0].x - 1) >= 0 && (piece[1].x - 1) >= 0) {
    if ((piece[0].x < piece[1].x && board[piece[0].x - 1][piece[0].y] === 0) || (piece[1].x < piece[0].x && board[piece[1].x - 1][piece[1].y] === 0) || (piece[0].x === piece[1].x && board[piece[0].x - 1][piece[0].y] === 0 && board[piece[1].x - 1][piece[1].y] === 0)) {
      if (piece[0].color !== 0) {
        board[piece[0].x][piece[0].y] = 0;
        piece[0].x = piece[0].x - 1;
      }
      if (piece[1].color !== 0) {
        board[piece[1].x][piece[1].y] = 0;
        piece[1].x = piece[1].x - 1;
      }
    }
  }
}

export function MovePieceRight(board: number[][], piece: Puyo[]) {
  //Check piece is not blocked by border or by another puyo
  if ((piece[0].x + 1) < BOARD_WIDTH && (piece[1].x + 1) < BOARD_WIDTH) {
    if ((piece[0].x > piece[1].x && board[piece[0].x + 1][piece[0].y] === 0) || (piece[1].x > piece[0].x && board[piece[1].x + 1][piece[1].y] === 0) || (piece[0].x === piece[1].x && board[piece[0].x + 1][piece[0].y] === 0  && board[piece[1].x + 1][piece[1].y] === 0)) {
      if (piece[0].color !== 0) {
        board[piece[0].x][piece[0].y] = 0;
        piece[0].x = piece[0].x + 1;
      }
      if (piece[1].color !== 0) {
        board[piece[1].x][piece[1].y] = 0;
        piece[1].x = piece[1].x + 1;
      }
    }
  }
}

export function RotatePiece(board: number[][], piece: Puyo[]) {
  //Only rotate is 2 puyos are falling
  if (piece[0].color !== 0 && piece[1].color !== 0) {
    //Piece falling Horizontally
    if (piece[0].y === piece[1].y) {
      //P1-P2↓
      if (piece[0].x < piece[1].x) {
        if (piece[1].y + 1 < BOARD_HEIGHT && board[piece[0].x][piece[0].y + 1] === 0) {
          //Free piece send P2 on bottom of P1
          board[piece[1].x][piece[1].y] = 0;
          piece[1].x = piece[0].x;
          piece[1].y = piece[0].y + 1;
        }
      }
      //↑P2-P1
      else {
        if (piece[1].y - 1 >= 0 && board[piece[0].x][piece[0].y - 1] === 0) {
          //Free piece send P2 on top of P1
          board[piece[1].x][piece[1].y] = 0;
          piece[1].x = piece[0].x;
          piece[1].y = piece[0].y - 1;
        }
      }
    }
    else {
      //P1
      //P2←
      if (piece[0].y < piece[1].y) {
        if (piece[1].x - 1 < 0 || board[piece[1].x - 1][piece[1].y - 1] !== 0) {
          //Left blocked piece send P2 on top of P1
          board[piece[1].x][piece[1].y] = 0;
          piece[1].x = piece[0].x;
          piece[1].y = piece[0].y - 1;
        }
        else {
          //Free piece send P2 to left of P1
          board[piece[1].x][piece[1].y] = 0;
          piece[1].x = piece[0].x - 1;
          piece[1].y = piece[0].y;
        }
      }
      else {
        //P2→
        //P1
        if (piece[1].x + 1 >= BOARD_WIDTH || board[piece[1].x + 1][piece[1].y + 1] !== 0) {
          if (board[piece[0].x][piece[0].y + 1] === 0) {
            //Right blocked piece send P2 on bottom of P1
            board[piece[1].x][piece[1].y] = 0;
            piece[1].x = piece[0].x;
            piece[1].y = piece[0].y + 1;
          }
        }
        else {
          if (board[piece[1].x + 1][piece[1].y + 1] === 0) {
            //Free piece send P2 to right of P1
            board[piece[1].x][piece[1].y] = 0;
            piece[1].x = piece[0].x + 1;
            piece[1].y = piece[0].y;
          }
        }
      }
    }
  }
}

function CheckBoardForCombos(board: number[][]): number {
  let score = CheckForCombos(board);
  //if score > 0 then there was a combo
  if (score > 0) {
    //check the board until there are no puyos falling
    while (CheckBoardAfterCombo(board)) {
      score += CheckForCombos(board);
    }
  }
  return score;
}

function CheckForCombos(board: number[][]): number {
  let score = 0;
  // Search on the board for chain of combos
  for (let y = board[0].length; y > board.length - 1; y--) {
    for (let x = 0; x < board.length; x++) {
      //Only search in avaliable position
      if (board[x][y] !== 0) {
        let comboChain = Array<Puyo>();
        ComboSearch(x, y, board[x][y], board, comboChain);

        //If >= 4 a combo was made
        if (comboChain.length >= 4) {
          score += Math.ceil((comboChain.length / 4) * comboChain.length);

          //Remove puyos from board
          for (let i = 0; i < comboChain.length; i++) {
            let x1 = comboChain[i].x;
            let y1 = comboChain[i].y;

            board[x1][y1] = 0;
          }
        }
      }
    }
  }
  return score;
}

function ComboSearch(x: number, y: number, color: number, board: number[][], comboChain: Puyo[]) {
  if (x < 0 || y < 0 || x >= BOARD_WIDTH || y >= BOARD_HEIGHT)
    return;

  //Don't search in the current position if its empty
  if (board[x][y] === 0)
    return;

  let newPuyo = true;
  //Only puyos with same color
  if (board[x][y] === color) {
    for (let i = 0; i < comboChain.length; i++) {
      //Verifying current piece is not duplicated
      if (comboChain[i].color === board[x][y] && comboChain[i].x === x && comboChain[i].y === y)
        newPuyo = false;
    }
    if (newPuyo) {
      //Add new puyo to combo chain
      let puyo = { x: x, y: y, color: board[x][y] };
      comboChain.push(puyo)
    }
    else {
      return;
    }
  }
  else {
    return;
  }

  //Check in all possible directions (Up, Down, Right, Left)	
  ComboSearch(x, y - 1, color, board, comboChain);
  ComboSearch(x, y + 1, color, board, comboChain);
  ComboSearch(x + 1, y, color, board, comboChain);
  ComboSearch(x - 1, y, color, board, comboChain);

  return;
}

function CheckBoardAfterCombo(board: number[][]): boolean {
  //Check is there are puyos falling after combo
  let isFalling = false;
  for (let y = BOARD_HEIGHT; y > -1; y--) {
    for (let x = 0; x < BOARD_WIDTH; x++) {
      //
      if (y + 1 < BOARD_HEIGHT && board[x][y] !== 0)
        if (board[x][y + 1] === 0) {
          board[x][y + 1] = board[x][y];
          board[x][y] = 0;
          isFalling = true;
        }
    }
  }
  return isFalling;
}