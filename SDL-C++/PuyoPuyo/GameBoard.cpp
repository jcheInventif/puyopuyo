#pragma
#include "GameBoard.h"


GameBoard* GameBoard::instance = NULL;

bool GameBoard::UpdateBoard(int command)
{
	switch (command)
	{
	case PIECE_DOWN:
	{
		//check for combos if the piece is no longer falling
		if (((piece[0].y + 1) >= BOARD_HEIGHT || (piece[1].y + 1) >= BOARD_HEIGHT) ||
			(board[piece[0].x][piece[0].y + 1].color != 0 && board[piece[1].x][piece[1].y + 1].color != 0))
		{
			score += CheckBoardForCombos();

			//generate new piece
			piece[0].color = rand() % 4 + 1;
			piece[0].x = 2;
			piece[0].y = -1;
			piece[1].color = rand() % 4 + 1;
			piece[1].x = 3;
			piece[1].y = -1;
		}
		else
		{
			//Piece falling Horizontally
			if (piece[0].y == piece[1].y)
			{
				if (board[piece[0].x][piece[0].y + 1].color != 0 && piece[0].color != 0)
				{
					board[piece[0].x][piece[0].y].color = piece[0].color;
					piece[0].color = 0;
				}
				if (board[piece[1].x][piece[1].y + 1].color != 0 && piece[1].color != 0)
				{
					board[piece[1].x][piece[1].y].color = piece[1].color;
					piece[1].color = 0;
				}
			}

			//Piece falling
			if (piece[0].color != 0)
			{
				if(piece[0].y >= 0)
					board[piece[0].x][piece[0].y].color = 0;
				
				piece[0].y = piece[0].y + 1;
			}

			if (piece[1].color != 0)
			{
				if(piece[1].y >= 0)
					board[piece[1].x][piece[1].y].color = 0;
				
				piece[1].y = piece[1].y + 1;
			}
		}
	}
		break;
	case PIECE_LEFT:
	{
		MovePieceLeft();
	}
	break;
	case PIECE_RIGHT:
	{
		MovePieceRight();
	}
	break;
	case PIECE_ROTATE:
	{
		RotatePiece();
	}
	break;
	default:
		break;
	}
	
	//Update board and piece
	if (piece[0].color != 0 && piece[0].y >= 0 && piece[0].y < BOARD_HEIGHT)
	{
		int x = piece[0].x;
		int y = piece[0].y;
		int color = piece[0].color;
		board[x][y].color = color;
	}
	if (piece[1].color != 0 && piece[1].y >= 0 && piece[1].y < BOARD_HEIGHT)
	{
		int x = piece[1].x;
		int y = piece[1].y;
		int color = piece[1].color;
		board[x][y].color = color;
	}

	//GameOver if the piece is out of bounds
	if ((piece[0].y == -1 || piece[1].y == -1) && (piece[0].x != piece[1].x) && (board[piece[0].x][0].color != 0 || board[piece[1].x][0].color != 0))
	{
		//GameOver
		return false;
	}

	return true;
}

int GameBoard::CheckBoardForCombos()
{
	int newScore = CheckForCombos();
	//if score > 0 then there was a combo
	if (newScore > 0)
	{
		//check the board until there are no puyos falling
		while (CheckBoardAfterCombo())
		{
			newScore += CheckForCombos();
		}
	}
	return newScore;
}

int GameBoard::CheckForCombos()
{
	int newScore = 0;
	// Search on the board for chain of combos
	for (int y = BOARD_HEIGHT - 1; y > -1; y--)
	{
		for (int x = 0; x < BOARD_WIDTH; x++)
		{
			//Only search in avaliable position
			if (board[x][y].color != 0)
			{
				vector<Puyo> comboChain;
				ComboSearch(x, y, board[x][y].color, comboChain);

				//If >= 4 a combo was made
				if (comboChain.size() >= 4)
				{
					newScore += int(ceil((comboChain.size() / 4) * comboChain.size()));

					//Remove puyos from board
					for (int i = 0; i < comboChain.size(); i++)
					{
						int x1 = comboChain[i].x;
						int y1 = comboChain[i].y;

						board[x1][y1].color = 0;
					}
				}
			}
		}
	}
	return newScore;
}


void GameBoard::ComboSearch(int x, int y, int color, vector<Puyo>& comboChain)
{
	if (x < 0 || y < 0 || x >= BOARD_WIDTH || y >= BOARD_HEIGHT)
		return;

	//Don't search in the current position if its empty
	if (board[x][y].color == 0)
		return;

	bool newPuyo = true;
	//Only pieces with same color
	if (board[x][y].color == color)
	{
		for (int i = 0; i < comboChain.size(); i++)
		{
			//Verifying current piece is not duplicated
			if (comboChain[i].color == board[x][y].color && comboChain[i].x == x && comboChain[i].y == y)
				newPuyo = false;

		}
		if (newPuyo)
		{
			//Add new puyo to combo chain
			Puyo puyo = Puyo();
			puyo.color = board[x][y].color;
			puyo.x = x;
			puyo.y = y;

			comboChain.push_back(puyo);
		}
		else
		{
			return;
		}
	}
	else
	{
		return;
	}

	//Check in all possible directions (Up, Down, Right, Left)	
	ComboSearch(x, y - 1, color, comboChain);
	ComboSearch(x, y + 1, color, comboChain);
	ComboSearch(x + 1, y, color, comboChain);
	ComboSearch(x - 1, y, color, comboChain);

	return;

}

bool GameBoard::CheckBoardAfterCombo()
{
	//Check is there are puyos falling after combo
	bool isfalling = false;
	for (int y = BOARD_HEIGHT; y > -1; y--)
	{
		for (int x = 0; x < BOARD_WIDTH; x++)
		{
			//
			if (y + 1 < BOARD_HEIGHT && board[x][y].color != 0)
				if (board[x][y + 1].color == 0)
				{
					board[x][y + 1].color = board[x][y].color;
					board[x][y].color = 0;
					isfalling = true;
				}
		}
	}
	return isfalling;
}



void GameBoard::ResetBoard()
{
	for (int x = 0; x < BOARD_WIDTH; x++)
	{
		for (int y = 0; y < BOARD_HEIGHT; y++)
		{
			board[x][y].color = 0;
		}
	}

	score = 0;
}

void GameBoard::GeneratePiece()
{
	//generate number between 1 and 4
	piece[0].color = rand() % 4 + 1;
	piece[0].x = 2;
	piece[0].y = -1;
	piece[1].color = rand() % 4 + 1;
	piece[1].x = 3;
	piece[1].y = -1;
}

void GameBoard::RotatePiece()
{
	//Only rotate is 2 puyos are falling
	if (piece[0].color != 0 && piece[1].color != 0)
	{
		//Piece falling Horizontally
		if (piece[0].y == piece[1].y)
		{
			//P1-P2
			if (piece[0].x < piece[1].x)
			{
				if (piece[1].y + 1 < BOARD_HEIGHT && board[piece[0].x][piece[0].y + 1].color == 0)
				{
					//Free piece send P2 on bottom of P1
					board[piece[1].x][piece[1].y].color = 0;
					piece[1].x = piece[0].x;
					piece[1].y = piece[0].y + 1;
				}
			}
			//P2-P1
			else
			{
				if (piece[1].y - 1 >= 0 && board[piece[0].x][piece[0].y - 1].color == 0)
				{
					//Free piece send P2 on top of P1
					board[piece[1].x][piece[1].y].color = 0;
					piece[1].x = piece[0].x;
					piece[1].y = piece[0].y - 1;
				}
			}
		}
		else {
			//P1
			//P2
			if (piece[0].y < piece[1].y) {
				if (piece[1].x - 1 < 0 || board[piece[1].x - 1][piece[1].y - 1].color != 0) {
					//Left blocked piece send P2 on top of P1
					board[piece[1].x][piece[1].y].color = 0;
					piece[1].x = piece[0].x;
					piece[1].y = piece[0].y - 1;
				}
				else {
					//Free piece send P2 to left of P1
					board[piece[1].x][piece[1].y].color = 0;
					piece[1].x = piece[0].x - 1;
					piece[1].y = piece[0].y;
				}
			}
			else {
				//P2
				//P1
				if (piece[1].x + 1 >= BOARD_WIDTH || board[piece[1].x + 1][piece[1].y + 1].color != 0)
				{
					if (board[piece[0].x][piece[0].y + 1].color == 0)
					{
						//Right blocked piece send P2 on bottom of P1
						board[piece[1].x][piece[1].y].color = 0;
						piece[1].x = piece[0].x;
						piece[1].y = piece[0].y + 1;
					}
				}
				else
				{
					if (board[piece[1].x + 1][piece[1].y + 1].color == 0)
					{
						//Free piece send P2 to right of P1
						board[piece[1].x][piece[1].y].color = 0;
						piece[1].x = piece[0].x + 1;
						piece[1].y = piece[0].y;
					}
				}
			}
		}
	}
}

void GameBoard::MovePieceLeft()
{
	//Check piece is not blocked by border or by another puyo
	if ((piece[0].x - 1) >= 0 && (piece[1].x - 1) >= 0) 
	{
		if ((piece[0].x < piece[1].x && board[piece[0].x - 1][piece[0].y].color == 0) || (piece[1].x < piece[0].x && board[piece[1].x - 1][piece[1].y].color == 0) || (piece[0].x == piece[1].x && board[piece[0].x - 1][piece[0].y].color == 0 && board[piece[1].x - 1][piece[1].y].color == 0))
		{
			if (piece[0].color != 0)
			{
				board[piece[0].x][piece[0].y].color = 0;
				piece[0].x = piece[0].x - 1;
			}
			if (piece[1].color != 0)
			{
				board[piece[1].x][piece[1].y].color = 0;
				piece[1].x = piece[1].x - 1;
			}
		}
	}
}

void GameBoard::MovePieceRight()
{
	//Check piece is not blocked by border or by another puyo
	if ((piece[0].x + 1) < BOARD_WIDTH && (piece[1].x + 1) < BOARD_WIDTH)
	{
		if ((piece[0].x > piece[1].x && board[piece[0].x + 1][piece[0].y].color == 0) || (piece[1].x > piece[0].x && board[piece[1].x + 1][piece[1].y].color == 0) || (piece[0].x == piece[1].x && board[piece[0].x + 1][piece[0].y].color == 0  && board[piece[1].x + 1][piece[1].y].color == 0))
		{
			if (piece[0].color != 0)
			{
				board[piece[0].x][piece[0].y].color = 0;
				piece[0].x = piece[0].x + 1;
			}
			if (piece[1].color != 0)
			{
				board[piece[1].x][piece[1].y].color = 0;
				piece[1].x = piece[1].x + 1;
			}
		}
	}
}

int GameBoard::GetScore()
{
	return score;
}
