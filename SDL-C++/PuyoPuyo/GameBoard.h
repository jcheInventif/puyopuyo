#include "Constants.h"
#include <stdlib.h>
#include <vector>
#include <cmath>

using namespace std;

class GameBoard {
private:
	struct Puyo
	{
		int x = 0;
		int y = 0;
		int color = 0;
	};

	static GameBoard* instance;


public:
	static GameBoard* getInstance() {
		if (!instance)
			instance = new GameBoard();

		return instance;
	}

	
	Puyo board[BOARD_WIDTH][BOARD_HEIGHT];
	Puyo piece[SIZE_PIECE];
	int score = 0;


	bool UpdateBoard(int command);
	void RotatePiece();
	void MovePieceLeft();
	void MovePieceRight();
	int CheckBoardForCombos();
	int CheckForCombos();
	void ComboSearch(int x, int y, int color, vector<Puyo>& comboChain);
	bool CheckBoardAfterCombo();
	void ResetBoard();
	void GeneratePiece();
	int GetScore();
};