#include <time.h>
#include <vector>
#include <string>
#include <ctype.h>
#include <windows.h>

// SDL Includes
#include "SDL.h"
#include "SDL_image.h"
#include "SDL_ttf.h"

#include "Constants.h"
#include "GameBoard.h"


SDL_Window *Window = NULL;
SDL_Surface *MainSurface = NULL;

// Sprites Surfaces
SDL_Surface *SPRITE_BORDER_H = NULL;
SDL_Surface* SPRITE_BORDER_V = NULL;
SDL_Surface *SPRITE_RED = NULL;
SDL_Surface* SPRITE_GREEN = NULL;
SDL_Surface* SPRITE_BLUE = NULL;
SDL_Surface* SPRITE_YELLOW = NULL;

// Misc
SDL_Surface *GameStart = NULL;
SDL_Surface *GameOver = NULL;
SDL_Surface* Mensaje = NULL;

TTF_Font* fontTurtlesBig = NULL;
TTF_Font* fontTimelessSmall = NULL;


// Misc Vars
bool Running = true;
SDL_Event event;
int gameState;

// Piece Falling time
ULONGLONG PTime = 0;
int PTime_MAX = 500;
int PTime_MAX_SPEED = 50;


bool bSpeedUpFallingPiece = false;

// Function Prototipes
SDL_Surface *LoadSurfaceFromFile(std::string filename);
void         DrawSurface(int x, int y, SDL_Surface* source, SDL_Surface* destination);
void         DrawBoard();
bool         LoadFiles();
void         UnloadFiles();
bool         Update();

using namespace std;