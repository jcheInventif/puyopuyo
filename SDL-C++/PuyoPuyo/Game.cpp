
#include "Game.h"


int main(int argc, char* args[])
{
	// Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return 1;
	}

	//Create window
	Window = SDL_CreateWindow("Puyo Puyo - by JCHE", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

	if (Window == NULL)
	{
		printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
		return 1;
	}

	// Create Main Surface
	MainSurface = SDL_GetWindowSurface(Window);
	
	//Init TTF(True Type Font)
	if (TTF_Init() == -1)
	{
		return false;
	}

	// Load Files
	if (LoadFiles() == true)
	{
		// error
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return 1;
	}

	// initialize random seed:
	srand((unsigned int)time(NULL));

	//Cleen Screen
	SDL_FillRect(MainSurface, NULL, SDL_MapRGB(MainSurface->format, 0xFF, 0xFF, 0xFF));

	//Set Game State
	gameState = GAME_START;

	// Start the Game Loop
	while (Running)
	{
		if (Update())
		{
			// Error ocurrent in update
			Running = false;
		}
		while (SDL_PollEvent(&event) && Running)
		{
			if (event.type == SDL_KEYDOWN)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT:
					GameBoard::getInstance()->UpdateBoard(PIECE_LEFT);
					break;
				case SDLK_RIGHT:
					GameBoard::getInstance()->UpdateBoard(PIECE_RIGHT);
					break;
				case SDLK_DOWN:
					bSpeedUpFallingPiece = true;
					break;
				case SDLK_UP:
					// Rotate
					GameBoard::getInstance()->UpdateBoard(PIECE_ROTATE);
					break;
				case SDLK_ESCAPE:
					Running = false;
					break;
				case SDLK_s:
				{
					//Start Game
					gameState = GAME_RUNNING;
					GameBoard::getInstance()->ResetBoard();
					GameBoard::getInstance()->GeneratePiece();
					PTime = GetTickCount64();
				}
					break;
				case SDLK_p:
					// Pause option
					if (gameState == GAME_RUNNING) {
						gameState = GAME_PAUSE;
						
					}
					else if(gameState == GAME_PAUSE) {
						gameState = GAME_RUNNING;
					}
						
					break;
				default:
					break;
				}

			}
			if (event.type == SDL_KEYUP)
			{
				switch (event.key.keysym.sym)
				{
				case SDLK_DOWN:
					bSpeedUpFallingPiece = false;
					break;
				}
			}

			// Event Close
			if (event.type == SDL_QUIT)
			{
				Running = false;
			}
		}
	}

	// Unload Files
	UnloadFiles();
	return 0;
}

bool Update()
{
	switch (gameState)
	{
		case GAME_START:
		{
			//Draw welcome screen
			std::string txt_welcome = "WELCOME TO PUYO PUYO GAME";
			Mensaje = TTF_RenderText_Solid(fontTurtlesBig, txt_welcome.c_str(), { 0, 0, 255 });
			DrawSurface((SCREEN_WIDTH / 2) - 220, 70, Mensaje, MainSurface);

			DrawSurface((SCREEN_WIDTH / 2) - 150, (SCREEN_HEIGHT / 2) - 200, GameStart, MainSurface);

			std::string txt_start = "PRESS 'S' TO START ";
			Mensaje = TTF_RenderText_Solid(fontTurtlesBig, txt_start.c_str(), {255, 0, 0});
			DrawSurface((SCREEN_WIDTH / 2) - 150, SCREEN_HEIGHT - 100, Mensaje, MainSurface);
		}
		break;
		case GAME_RUNNING:
		{
			//Cleen Screen
			SDL_FillRect(MainSurface, NULL, SDL_MapRGB(MainSurface->format, 0xFF, 0xFF, 0xFF));

			
			ULONGLONG tDifference = (GetTickCount64()) - PTime;
			if (tDifference >= PTime_MAX || (bSpeedUpFallingPiece && (tDifference >= PTime_MAX_SPEED)))
			{
				PTime = GetTickCount64();
				if (!GameBoard::getInstance()->UpdateBoard(PIECE_DOWN))
				{
					gameState = GAME_OVER;
				}
			}

			DrawBoard();
		}
		break;
		case GAME_PAUSE:
		{
			std::string txt_pause = " Pause ";
			Mensaje = TTF_RenderText_Solid(fontTurtlesBig, txt_pause.c_str(), {194, 24, 7});
			DrawSurface((SCREEN_WIDTH / 2) - 40, SCREEN_HEIGHT / 2, Mensaje, MainSurface);
		}
		break;
		case GAME_OVER:
		{
			//Cleen Screen
			SDL_FillRect(MainSurface, NULL, SDL_MapRGB(MainSurface->format, 0xFF, 0xFF, 0xFF));
			DrawSurface((SCREEN_WIDTH / 2) - 200, (SCREEN_HEIGHT / 2) - 250, GameOver, MainSurface);

			std::string txt_start = "PRESS 'S' TO START AGAIN";
			Mensaje = TTF_RenderText_Solid(fontTurtlesBig, txt_start.c_str(), { 0, 0, 0 });
			DrawSurface((SCREEN_WIDTH / 2) - 200, SCREEN_HEIGHT / 2, Mensaje, MainSurface);

			std::string txt_or = "OR";
			Mensaje = TTF_RenderText_Solid(fontTurtlesBig, txt_or.c_str(), { 0, 0, 0 });
			DrawSurface((SCREEN_WIDTH / 2) - 30, (SCREEN_HEIGHT / 2) + 50, Mensaje, MainSurface);

			std::string txt_exit = "PRESS 'ESC' TO EXIT";
			Mensaje = TTF_RenderText_Solid(fontTurtlesBig, txt_exit.c_str(), { 0, 0, 0 });
			DrawSurface((SCREEN_WIDTH / 2) - 150, (SCREEN_HEIGHT / 2) + 100, Mensaje, MainSurface);
		}
		break;
		default:
			break;
	}

	// Update the Screen
	if (SDL_UpdateWindowSurface(Window) == -1)
	{
		//Error
		printf("SDL could not Update the surface! SDL_Error: %s\n", SDL_GetError());
		return true;
	}
	return false;
}

void DrawBoard()
{
	// vals
	int px_start = MATRIX_DRAW_POS_X + SPRITE_SIZE;
	int py_start = MATRIX_DRAW_POS_Y;
	int rightBorderPosX = px_start + (BOARD_WIDTH * SPRITE_SIZE);
	int leftBorderPosX = MATRIX_DRAW_POS_X;
	int DownBorderMaxX = px_start + (BOARD_WIDTH * SPRITE_SIZE);
	int YLimit = (BOARD_HEIGHT * SPRITE_SIZE) + (SPRITE_SIZE * 2); // * 2 borders up and down

	// Draw Borders
	// Down Border
	for (int xp = MATRIX_DRAW_POS_X; xp < DownBorderMaxX; xp += SPRITE_SIZE)
	{
		DrawSurface(xp, YLimit - SPRITE_SIZE, SPRITE_BORDER_H, MainSurface);
	}

	//UP Border
	for (int xp = MATRIX_DRAW_POS_X; xp < DownBorderMaxX; xp += SPRITE_SIZE)
	{
		DrawSurface(xp, py_start, SPRITE_BORDER_H, MainSurface);
	}

	// Left Border
	for (int yp = py_start; yp < YLimit; yp += SPRITE_SIZE) 
	{
		DrawSurface(MATRIX_DRAW_POS_X, yp, SPRITE_BORDER_V, MainSurface);
	}

	// Right Border
	for (int yp = py_start; yp < YLimit; yp += SPRITE_SIZE)
	{
		DrawSurface(rightBorderPosX, yp, SPRITE_BORDER_V, MainSurface);
	}

	// Draw Board
	for (int x = 0; x < BOARD_WIDTH; x++)
	{
		for (int y = 0; y < BOARD_HEIGHT; y++)
		{
			if (GameBoard::getInstance()->board[x][y].color != 0)
			{
				int PosXG = px_start + (x * SPRITE_SIZE);
				int PosYG = py_start + ((y + 1) * SPRITE_SIZE);
				
				if (GameBoard::getInstance()->board[x][y].color == TCOLOR_BLUE)
				{
					DrawSurface(PosXG, PosYG, SPRITE_BLUE, MainSurface);
				}

				if (GameBoard::getInstance()->board[x][y].color == TCOLOR_GREEN)
				{
					DrawSurface(PosXG, PosYG, SPRITE_GREEN, MainSurface);
				}

				if (GameBoard::getInstance()->board[x][y].color == TCOLOR_RED)
				{
					DrawSurface(PosXG, PosYG, SPRITE_RED, MainSurface);
				}
				if (GameBoard::getInstance()->board[x][y].color == TCOLOR_YELLOW)
				{
					DrawSurface(PosXG, PosYG, SPRITE_YELLOW, MainSurface);
				}
			}
		}
	}

	//Draw Score
	std::string txt_score = "Score: " + std::to_string(GameBoard::getInstance()->GetScore());
	Mensaje = TTF_RenderText_Solid(fontTurtlesBig, txt_score.c_str(), { 255, 0, 0 });
	DrawSurface((SCREEN_WIDTH / 2) - 100, YLimit + 50, Mensaje, MainSurface);


	//Draw Info
	std::string txt_left_arrow = "Left Arrow";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_left_arrow.c_str(), { 0, 0, 0 });
	DrawSurface(2, (SCREEN_HEIGHT / 2) - 160, Mensaje, MainSurface);

	std::string txt_left_mensaje = "Move Left";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_left_mensaje.c_str(), { 0, 0, 0 });
	DrawSurface(2, (SCREEN_HEIGHT / 2) - 140, Mensaje, MainSurface);

	std::string txt_right_arrow = "Right Arrow";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_right_arrow.c_str(), { 0, 0, 0 });
	DrawSurface(2, (SCREEN_HEIGHT / 2) - 80, Mensaje, MainSurface);

	std::string txt_right_mensaje = "Move Right";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_right_mensaje.c_str(), { 0, 0, 0 });
	DrawSurface(2, (SCREEN_HEIGHT / 2) - 60, Mensaje, MainSurface);

	std::string txt_key_s = "Key S";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_key_s.c_str(), { 0, 0, 0 });
	DrawSurface(2, (SCREEN_HEIGHT / 2), Mensaje, MainSurface);

	std::string txt_key_s_mensaje = "New Game";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_key_s_mensaje.c_str(), { 0, 0, 0 });
	DrawSurface(2, (SCREEN_HEIGHT / 2) + 20, Mensaje, MainSurface);


	//Info Right
	int infoRightPos = SPRITE_SIZE + rightBorderPosX + 2;
	std::string txt_up_arrow = "Up Arrow";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_up_arrow.c_str(), { 0, 0, 0 });
	DrawSurface(infoRightPos, (SCREEN_HEIGHT / 2) - 160, Mensaje, MainSurface);

	std::string txt_up_mensaje = "Rotate";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_up_mensaje.c_str(), { 0, 0, 0 });
	DrawSurface(infoRightPos, (SCREEN_HEIGHT / 2) - 140, Mensaje, MainSurface);

	std::string txt_down_arrow = "Down Arrow";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_down_arrow.c_str(), { 0, 0, 0 });
	DrawSurface(infoRightPos, (SCREEN_HEIGHT / 2) - 80, Mensaje, MainSurface);

	std::string txt_down_mensaje = "Speed Up";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_down_mensaje.c_str(), { 0, 0, 0 });
	DrawSurface(infoRightPos, (SCREEN_HEIGHT / 2) - 60, Mensaje, MainSurface);

	std::string txt_key_p = "Key P";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_key_p.c_str(), { 0, 0, 0 });
	DrawSurface(infoRightPos, (SCREEN_HEIGHT / 2), Mensaje, MainSurface);

	std::string txt_key_p_mensaje = "Play/Pause";
	Mensaje = TTF_RenderText_Solid(fontTimelessSmall, txt_key_p_mensaje.c_str(), { 0, 0, 0 });
	DrawSurface(infoRightPos, (SCREEN_HEIGHT / 2) + 20, Mensaje, MainSurface);
}

bool LoadFiles()
{
	//Loading Sprites Surfaces
	SPRITE_BORDER_H = LoadSurfaceFromFile("assets/borderH.png");
	SPRITE_BORDER_V = LoadSurfaceFromFile("assets/borderV.png");
	SPRITE_GREEN = LoadSurfaceFromFile("assets/puyoGreenO.png");
	SPRITE_RED = LoadSurfaceFromFile("assets/puyoRedO.png");
	SPRITE_YELLOW = LoadSurfaceFromFile("assets/puyoYellowO.png");
    SPRITE_BLUE = LoadSurfaceFromFile("assets/puyoBlueO.png");
	
	GameStart = LoadSurfaceFromFile("assets/gameStart.jpg");
	GameOver = LoadSurfaceFromFile("assets/gameOver.png");

	fontTurtlesBig = TTF_OpenFont("assets/Turtles.otf", 27);
	fontTimelessSmall = TTF_OpenFont("assets/Timeless.ttf", 16);


	return false;
}

void UnloadFiles()
{
	// Unload Sprites Surfaces
	SDL_FreeSurface(SPRITE_BORDER_H);
	SDL_FreeSurface(SPRITE_BORDER_V);
	SDL_FreeSurface(SPRITE_RED);
	SDL_FreeSurface(SPRITE_GREEN);
	SDL_FreeSurface(SPRITE_BLUE);
	SDL_FreeSurface(SPRITE_YELLOW);

	SDL_FreeSurface(Mensaje);
	SDL_FreeSurface(GameStart);
	SDL_FreeSurface(GameOver);
	
	TTF_CloseFont(fontTurtlesBig);
	TTF_CloseFont(fontTimelessSmall);
	TTF_Quit();
	SDL_Quit();

}

void DrawSurface(int x, int y, SDL_Surface* source, SDL_Surface* destination)
{
	// Create Surface Rectangle
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;

	// Blit
	SDL_BlitSurface(source, NULL, destination, &offset);
}

SDL_Surface *LoadSurfaceFromFile(std::string filename)
{
	//The final optimized image
	SDL_Surface* optimizedSurface = NULL;

	//Load image at specified path
	SDL_Surface* loadedSurface = IMG_Load(filename.c_str());
	if (loadedSurface == NULL)
	{
		printf("Unable to load image %s! SDL_image Error: %s\n", filename.c_str(), IMG_GetError());
	}
	else
	{
		//Convert surface to screen format
		optimizedSurface = SDL_ConvertSurfaceFormat(loadedSurface, SDL_PIXELFORMAT_RGBA8888, 0);
		if (optimizedSurface == NULL)
		{
			printf("Unable to optimize image %s! SDL Error: %s\n", filename.c_str(), SDL_GetError());
		}

		//Get rid of old loaded surface
		SDL_FreeSurface(loadedSurface);
	}

	return optimizedSurface;
}

// END


